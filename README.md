# Assignment 1

## Problem 1

<img src="https://jaxenter.com/wp-content/uploads/2019/09/shutterstock_551771506.jpg" alt="drawing" width="600"/>

Create new Ballerina Project
```
ballerina new problem1
```

Move to folder
```
cd problem1
```

```
ballerina openapi gen-contract learning_environment -i main.bal
```

## Problem 2

**Running Program Video below**

[![Watch the video](https://dz2cdn1.dzone.com/storage/temp/13772863-1mkxsragdwfbtsmehw8uckq.jpeg)](multimedia/problem2.mp4)

- Execute the command below in the Ballerina tools distribution to generate the mock service.
```
ballerina grpc --input assembly_service.proto --mode service --output service
```

- Execute the command below in the Ballerina tools distribution to generate the stub file.
```
ballerina grpc --input assembly_service.proto
```

**Ballerina Documentation on building Server reference [Link](https://ballerina.io/1.2/learn/by-example/grpc-server-streaming.html?is_ref_by_example=true).**

- Execute the following command to build the 'server' module.
```
ballerina build server
```

- Run the server using the following command.
```
ballerina run target/bin/server.jar
```

**Ballerina Documentation on building Client reference [Link](https://ballerina.io/1.2/learn/by-example/grpc-client-streaming.html?is_ref_by_example=true).**

- Execute the following command to build the 'client' module.
```
ballerina build client
```

- Run the client using the following command.
```
ballerina run target/bin/client.jar
```
