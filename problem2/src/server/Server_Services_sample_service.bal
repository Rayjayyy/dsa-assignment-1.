import ballerina/grpc;
import ballerina/io;

listener grpc:Listener ep = new (9090);

Address addr_1 = {region: "Windhoek",country: "Namibia"};
Address addr_2 = {region: "Kunene",country: "Namibia"};

func func_1 = {
    func_name: "extraPDF",
    language: "Python",
    keywords: ["PDF"],
    developer_name: "Jose Quenum",
    email: "jquenum@nust.na",
    address: addr_1,
    func_version: 1
};

func func_2 = {
    func_name: "extraPDF",
    language: "Python",
    keywords: ["PDF"],
    developer_name: "Jose Quenum",
    email: "jquenum@nust.na",
    address: addr_1,
    func_version: 2
};

func func_3 = {
    func_name: "login",
    language: "Java",
    keywords: ["Username", "Password"],
    developer_name: "Uakomba Mbasuva",
    email: "umbasuva@nust.na",
    address: addr_2,
    func_version: 1
};

func[] listOfFunctions = [func_1, func_2, func_3];

service Server_Services on ep {

    resource function add_new_fn(grpc:Caller caller, func new_function) {
        io:println("----------- add_new_fn service invoked -----------");
		error? result = ();
		int currVersion = 0;
        foreach func function_ in listOfFunctions {
            if function_.func_name == new_function.func_name {
                currVersion = function_.func_version;
                if function_.func_version == new_function.func_version {
					result = caller->send("Version of Function already exists");
                }
            }
        }
        int expectedVersion = currVersion + 1;
        if new_function.func_version != expectedVersion {
			result = caller->send("Wrong version number! Expected: " + expectedVersion.toString());
        }
        listOfFunctions.push(new_function);
		string res = "Function " + new_function.func_name + " has been successfully added to Assembly System";
		io:println(res);
		result = caller->send(res);
		io:println("----------- add_new_fn service completed -----------");
    }
	
	@grpc:ResourceConfig { streaming: true } 
    resource function add_fns(grpc:Caller caller, multiple_functions multiple_func) {
        io:println("----------- add_fns service invoked -----------");
		error? result = ();
		foreach func[] new_function_list in multiple_func {
			foreach func new_function in new_function_list {
				int currVersion = 0;
				foreach func function_ in listOfFunctions {
					if function_.func_name == new_function.func_name {
						currVersion = function_.func_version;
						if function_.func_version == new_function.func_version {
							result = caller->send("Version of Function already exists");
						}
					}
				}
				
				int expectedVersion = currVersion + 1;
				if new_function.func_version != expectedVersion {
					result = caller->send("Wrong version number! Expected: " + expectedVersion.toString());
				}
				listOfFunctions.push(new_function);
				string res = "Function " + new_function.func_name + " has been successfully added to Assembly System";
				io:println(res);
				result = caller->send(res);
			}
        }
		io:println("----------- add_fns service completed -----------");
    }
	
    resource function delete_fn(grpc:Caller caller, request_function del_func) {
        io:println("----------- delete_fn service invoked -----------");
		error? result = ();
		boolean deleted = false;
		
		int i = 0;
        while i < listOfFunctions.length() {
			if (listOfFunctions[i].func_name == del_func.func_name) && (listOfFunctions[i].func_version.toString() == del_func.func_version) {   
				func err = listOfFunctions.remove(i);
				deleted = true;
            }
			i += 1;
        }
        if deleted {
			result = caller->send(del_func.func_name + " has sucessfully been deleted");
        }else{
			result = caller->send("Function "+ del_func.func_name +" not found");
		}
		io:println("----------- delete_fn service completed -----------");
    }
	
    resource function show_fn(grpc:Caller caller, request_function request_func) {
        io:println("----------- show_fn service invoked -----------");
		error? result = ();
		boolean found = false;
		
		foreach func function_ in listOfFunctions {
            if function_.func_name == request_func.func_name && function_.func_version.toString() == request_func.func_version {
                found = true;
				result = caller->send(function_);
            }
        }
		if found == false {
			result = caller->send(request_func.func_name + " not found");
        }
        io:println("----------- show_fn service completed -----------");
    }
	
    @grpc:ResourceConfig { streaming: true } 
    resource function show_all_fns(grpc:Caller caller, string name) {
        io:println("----------- show_all_fns service invoked -----------");
		error? result = ();
		foreach func function_ in listOfFunctions {
            result = caller->send(function_.toString());
        }
		io:println("----------- show_all_fns service completed -----------");
    }
	
	@grpc:ResourceConfig { streaming: true } 
    resource function show_all_with_criteria(grpc:Caller caller, criteria_request criteria_func) {
        io:println("----------- show_all_with_criteria service invoked -----------");
		error? result = ();
		foreach func function_ in listOfFunctions {
            if function_.language == criteria_func.language {
				//string[] req_keywords = criteria_func.keywords;
				foreach string key_ in function_.keywords {
					io:println(function_.toString()+ "\n--------------");
					result = caller->send(function_.toString());
				}
			}
        }
		io:println("----------- show_all_with_criteria service completed -----------");        
    }
}

public type Address record {|
    string region = "";
    string country = "";
    
|};

public type func record {|
    string func_name = "";
    string language = "";
    string[] keywords = [];
    string developer_name = "";
    string email = "";
    Address? address = ();
    int func_version = 0;
    
|};

public type request_function record {|
    string func_name = "";
    string func_version = "";
    
|};

public type multiple_functions record {|
    func[] functions = [];
    
|};

public type criteria_request record {|
    string language = "";
    string[] keywords = [];
    
|};

const string ROOT_DESCRIPTOR = "0A16617373656D626C795F736572766963652E70726F746F1207736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223B0A074164647265737312160A06726567696F6E1801200128095206726567696F6E12180A07636F756E7472791802200128095207636F756E74727922E7010A0466756E63121B0A0966756E635F6E616D65180120012809520866756E634E616D65121A0A086C616E677561676518022001280952086C616E6775616765121A0A086B6579776F72647318032003280952086B6579776F72647312250A0E646576656C6F7065725F6E616D65180420012809520D646576656C6F7065724E616D6512140A05656D61696C1805200128095205656D61696C122A0A076164647265737318062001280B32102E736572766963652E4164647265737352076164647265737312210A0C66756E635F76657273696F6E180720012805520B66756E6356657273696F6E22520A10726571756573745F66756E6374696F6E121B0A0966756E635F6E616D65180120012809520866756E634E616D6512210A0C66756E635F76657273696F6E180220012809520B66756E6356657273696F6E22410A126D756C7469706C655F66756E6374696F6E73122B0A0966756E6374696F6E7318012003280B320D2E736572766963652E66756E63520966756E6374696F6E73224A0A1063726974657269615F72657175657374121A0A086C616E677561676518012001280952086C616E6775616765121A0A086B6579776F72647318022003280952086B6579776F72647332B2030A0F5365727665725F536572766963657312390A0A6164645F6E65775F666E120D2E736572766963652E66756E631A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512460A076164645F666E73121B2E736572766963652E6D756C7469706C655F66756E6374696F6E731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565300112440A0964656C6574655F666E12192E736572766963652E726571756573745F66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512330A0773686F775F666E12192E736572766963652E726571756573745F66756E6374696F6E1A0D2E736572766963652E66756E63124C0A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565300112530A1673686F775F616C6C5F776974685F637269746572696112192E736572766963652E63726974657269615F726571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75653001620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "assembly_service.proto":"0A16617373656D626C795F736572766963652E70726F746F1207736572766963651A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F223B0A074164647265737312160A06726567696F6E1801200128095206726567696F6E12180A07636F756E7472791802200128095207636F756E74727922E7010A0466756E63121B0A0966756E635F6E616D65180120012809520866756E634E616D65121A0A086C616E677561676518022001280952086C616E6775616765121A0A086B6579776F72647318032003280952086B6579776F72647312250A0E646576656C6F7065725F6E616D65180420012809520D646576656C6F7065724E616D6512140A05656D61696C1805200128095205656D61696C122A0A076164647265737318062001280B32102E736572766963652E4164647265737352076164647265737312210A0C66756E635F76657273696F6E180720012805520B66756E6356657273696F6E22520A10726571756573745F66756E6374696F6E121B0A0966756E635F6E616D65180120012809520866756E634E616D6512210A0C66756E635F76657273696F6E180220012809520B66756E6356657273696F6E22410A126D756C7469706C655F66756E6374696F6E73122B0A0966756E6374696F6E7318012003280B320D2E736572766963652E66756E63520966756E6374696F6E73224A0A1063726974657269615F72657175657374121A0A086C616E677561676518012001280952086C616E6775616765121A0A086B6579776F72647318022003280952086B6579776F72647332B2030A0F5365727665725F536572766963657312390A0A6164645F6E65775F666E120D2E736572766963652E66756E631A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512460A076164645F666E73121B2E736572766963652E6D756C7469706C655F66756E6374696F6E731A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565300112440A0964656C6574655F666E12192E736572766963652E726571756573745F66756E6374696F6E1A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512330A0773686F775F666E12192E736572766963652E726571756573745F66756E6374696F6E1A0D2E736572766963652E66756E63124C0A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565300112530A1673686F775F616C6C5F776974685F637269746572696112192E736572766963652E63726974657269615F726571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75653001620670726F746F33",
        "google/protobuf/wrappers.proto":"0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33"
        
    };
}