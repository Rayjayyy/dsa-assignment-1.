import ballerina/io;
import ballerina/log;
import ballerina/grpc;
import ballerina/lang.'int as ints;

public function main (string... args) {

    Server_ServicesBlockingClient blockingEp = new("http://localhost:9090");
	
	io:println("-------- MENU --------\n1. add_new_fn: \n2. add_fns: \n3. delete_fn: \n4. show_fn:  \n5. show_all_fns: \n6. show_all_with_criteria: \n----------------------");
    
    string response = io:readln("Enter choice: ");    

    boolean _1 = response.equalsIgnoreCaseAscii("1");
    boolean _2 = response.equalsIgnoreCaseAscii("2");
    boolean _3 = response.equalsIgnoreCaseAscii("3");
	boolean _4 = response.equalsIgnoreCaseAscii("4");
	boolean _5 = response.equalsIgnoreCaseAscii("5");
	boolean _6 = response.equalsIgnoreCaseAscii("6");
	boolean _7 = response.equalsIgnoreCaseAscii("7");
	
	if (_1 == true) {
		action_add_new_fn(blockingEp);
	}else if (_2) {
		action_add_fns();
	}else if (_3) {
		action_delete_fn(blockingEp);
	}else if (_4) {
		 action_show_fn(blockingEp);
	}else if (_5) {
		 action_show_all_fns();
	}else if (_6) {
		 action_show_all_with_criteria();
	}
}

//===============================================================================

public function action_add_new_fn(Server_ServicesBlockingClient blockingEp){
	io:println("------- ENTER NEW FUNCTION DETAILS --------");
	
	func new_func = get_function_details();
	
	var addResponse = blockingEp->add_new_fn(new_func);		
	if (addResponse is error) {
		log:printError("Connector Error experienced");
	} else {
		string result;
		grpc:Headers resHeaders;
		[result, resHeaders] = addResponse;
		io:println(result);
	}
	io:println("------------ COMPLETE -------------");
}

//===============================================================================

// Message listener for incoming string messages
service listener_string = service {
    resource function onMessage(string rec) {
        io:println(rec);
    }
    resource function onError(error err) {
        io:println("Error from Connector");
    }
    resource function onComplete() {
        io:println("Server completes sending responses");
    }
};

//===============================================================================

public function action_add_fns(){
	Server_ServicesClient nonBlockingEp = new("http://localhost:9090");
	string count = io:readln("Enter number of functions you want to enter: ");
	int|error count_ = ints:fromString(count);
	
	func[] list_funcs = [];
	
	if (count_ is int) {
		foreach var i in 1...count_ {
			io:println("------------- ENTER FUNCTION "+ i.toString()+" DETAILS ------------------");
			func new_func = get_function_details();
			list_funcs.push(new_func);		
		}
		
		multiple_functions multiple_funcs = {
			functions: list_funcs
		};
		
		grpc:Error? response = nonBlockingEp->add_fns(multiple_funcs, listener_string);
		if (response is grpc:Error){
			log:printError("Error from Connector");
		}else{
			io:println("Connected successfully");
		}
    } else {
        io:println("error input: ", count_.detail());
    }				
}

//===============================================================================

public function get_function_details() returns func {
	string funName = io:readln("Function name: ");
	string developerName = io:readln("Developer name: ");
	string email = io:readln("Email: ");
	string reg_ = io:readln("Address Country: ");
	string country_ = io:readln("Address Region: ");
	string language = io:readln("Enter programming language: ");
	
	Address new_address = {
		region: reg_,
        country: country_
	};
	
	func new_func = {
		func_name: funName,
		language: language,
		developer_name: developerName,
		email: email,
		address: new_address,
		func_version: 1
	};
	return <@untained>new_func;
}

//===============================================================================

public function action_delete_fn(Server_ServicesBlockingClient blockingEp){
	string funName = io:readln("Enter Function name: ");
	string version_ = io:readln("Enter version: ");
		
	request_function request_func = {
        func_name: funName,
        func_version: version_
    };
	
	var addResponse = blockingEp->delete_fn(request_func);		
	if (addResponse is error) {
		log:printError("Error from Connector");
	} else {
		string result;
		grpc:Headers resHeaders;
		[result, resHeaders] = addResponse;
		io:println("Response - " + result + "\n");
	}
}

//===============================================================================

public function action_show_fn(Server_ServicesBlockingClient blockingEp){
	string funName = io:readln("Enter Function name: ");
	string version_ = io:readln("Enter version: ");
	
	request_function request_func = {
        func_name: funName,
        func_version: version_
    };
	
	var addResponse = blockingEp->show_fn(request_func);		
	if (addResponse is error) {
		log:printError("Connector Error experienced");
	} else {
		func result_func;
		grpc:Headers resHeaders;
		[result_func, resHeaders] = addResponse;
		io:println(result_func);
	}
	io:println("------------ COMPLETE -------------");
}

//===============================================================================

public function action_show_all_fns(){
	Server_ServicesClient nonBlockingEp = new("http://localhost:9090");
	
	grpc:Error? response = nonBlockingEp->show_all_fns("", listener_string);
	if (response is grpc:Error){
		log:printError("Error from Connector");
	}else{
		io:println("Connected successfully");
	}
}

//===============================================================================

public function action_show_all_with_criteria(){
	Server_ServicesClient nonBlockingEp = new("http://localhost:9090");
	
	string lang_ = io:readln("Enter implemented language: ");	
	
	criteria_request criteria_func = {
        language: lang_,
        keywords: []
    };
	
	grpc:Error? response = nonBlockingEp->show_all_with_criteria(criteria_func, listener_string);
	if (response is grpc:Error){
		log:printError("Error from Connector");
	}else{
		io:println("Connected successfully");
	}
}

//===============================================================================