import ballerina/http;

//=====================================
//============Client Config============
//=====================================
public type virtuallearningenvironmentapplicationClientConfig record {
    string serviceUrl;
    http:ClientConfiguration clientConfig;
};

//==============================
//============Client============
//==============================
public type virtuallearningenvironmentapplicationClient client object {
    public http:Client clientEp;
    public virtuallearningenvironmentapplicationClientConfig config;

    public function __init(virtuallearningenvironmentapplicationClientConfig config) {
        http:Client httpEp = new(config.serviceUrl, {auth: config.clientConfig.auth, cache: config.clientConfig.cache});
        self.clientEp = httpEp;
        self.config = config;
    }

    public remote function 'resource1(objectLearner 'resource1Body) returns http:Response | error {
        http:Client 'resource1Ep = self.clientEp;
        http:Request request = new;
        json 'resource1JsonBody = check json.constructFrom('resource1Body);
        request.setPayload('resource1JsonBody);

        // TODO: Update the request as needed
        return check 'resource1Ep->post("/learner/add", request);
    }
    
    public remote function 'resource2(objectLearner 'resource2Body) returns http:Response | error {
        http:Client 'resource2Ep = self.clientEp;
        http:Request request = new;
        json 'resource2JsonBody = check json.constructFrom('resource2Body);
        request.setPayload('resource2JsonBody);

        // TODO: Update the request as needed
        return check 'resource2Ep->post("/learner/update", request);
    }
    
    public remote function 'resource3(string learner) returns http:Response | error {
        http:Client 'resource3Ep = self.clientEp;
        http:Request request = new;

        // TODO: Update the request as needed
        return check 'resource3Ep->get("/studyingMaterials/{learner}", message = request);
    }
    
};
